__version__ = '3.4.0'
__name__ = 'amy'

from .plugin import Plugin
from .message import Message
from .instance import instance, Instance, initSuper
