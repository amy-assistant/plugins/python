var jackrabbit = require("jackrabbit");

var rabbit = jackrabbit("amqp://159.89.24.207");
// var rabbit = jackrabbit("amqp://127.0.0.1");
var exchange = rabbit.default();
var rpc = exchange.queue({
    name: "messenger",
    prefetch: 1,
    durable: true
});

function asyncRPC(exchange, queue, body, headers = {}) {
    return new Promise((res, rej) => {
        try {
            exchange.publish(body, {
                key: queue,
                reply: res,
                headers
            })
        } catch (e) {
            rej(e)
        }
    })
}







class UserService {
    constructor(rabbit) {
        this.exchange = rabbit.default();
    }
    create(username, platform) {
        return asyncRPC(this.exchange, platform, {}, {
            username,
            'action': 'create'
        })
    }
    authorize(username, platform, token) {
        return asyncRPC(this.exchange, platform, {
            token
        }, {
            username,
            'action': 'authorize'
        })
    }
    start(username, platform) {
        return asyncRPC(this.exchange, platform, {}, {
            username,
            'action': 'start'
        })
    }

    send(username, platform, message) {
        return asyncRPC(this.exchange, platform, {
            message
        }, {
            username,
            'action': 'send'
        })
    }
    status(username, platform) {
        return asyncRPC(this.exchange, platform, {}, {
            username,
            'action': 'status'
        })
    }
}

const us = new UserService(rabbit)


us.create('max.aussprung@gmail.com', 'messenger').then(data => {
    console.log("result:", data);
})

us.authorize('max.aussprung@gmail.com', 'messenger', 'yb3rHf6LSZ4GzdS').then(data => {
    console.log("result:", data);
})

testmessage = {
    platform: 'messenger',
    content: 'test over js',
    sender: '',
    channel: '2120998508017180',
    datetime: Date.now(),
    user: 'max.aussprung@gmail.com',
    out: true
}

// us.send('max.aussprung@gmail.com', 'messenger', testmessage).then(data => {
//     console.log("result:", data);
// })

us.status('max.aussprung@gmail.com', 'messenger').then(data => {
    console.log("result:", data);
})
