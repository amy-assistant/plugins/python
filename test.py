import json
from amy import Plugin, Message, instance
from fbchat.models import Message as FBMessage, ThreadType
from fbchat import Client
import datetime

import os

os.environ['PRODUCTION'] = '1'
os.environ['AMY_Q_HOST'] = '159.89.24.207'


@instance
class Messenger(Client):

    def onCreate(self, context, username, session):
        self.context = context
        try:
            Client.__init__(self, username, 'token',
                            session_cookies=session and json.loads(session), max_tries=1)
        except:
            pass
        

    def onAuth(self, context, token):
        try:
            self.login(self.email, token,  max_tries=3)
            return json.dumps(self.getSession())
        except:
            return None
        

    def isAuthorized(self):
        if hasattr(self, 'req_url'):
            return self.isLoggedIn()
        return False

    def onStart(self, context):
        context.startThread(self, self.listen)

    def onStop(self, context):
        self.stopListening()
        self.logout()
        context.stopThread(self)

    def onMessage(self, author_id, message_object, thread_id, thread_type, **kwargs):
        self.markAsDelivered(thread_id, message_object.uid)
        self.markAsRead(thread_id)

        message = Message() \
            .setPlatform('messenger') \
            .setContent(message_object.text) \
            .setSender(author_id) \
            .setChannel(thread_id) \
            .setDatetime(datetime.datetime.fromtimestamp(message_object.timestamp//1000).isoformat()) \
            .setUser(self.email).setOut(self.uid == author_id)

        self.context.publishMessage(message)

    def onSendMessage(self, context, message):
        thread = self.fetchThreadInfo(message['channel'])[message['channel']]
        self.send(FBMessage(
            text=message['content']), thread_id=message['channel'], thread_type=thread.type)

    def onDelete(self, context):
        if hasattr(self, 'req_url'):
            self.stopListening()
            self.logout()
        
        context.stopThread(self)
       


def main():
    facebook = Plugin(name='messenger', instanceCls=Messenger)


if __name__ == "__main__":
    main()

